@extends('layouts.app')

@section('content')
<div class="container">
    <form action="/editSiswa" method="post">
        @csrf
        <div class="card">
        <div class="card-body">
         <h3>Profile</h3>
            <input type="hidden" name="id" value="{{ $data->id }}">
            <div class="mb-3">
            <label for="" class="form-label">Nama Lengkap</label>
            <input type="text" class="form-control" name="name" id="" value="{{ $data->name }}">
            </div>
            <div class="mb-3">
            <label for="" class="form-label">NIS</label>
            <input type="text" class="form-control" name="nis" id="" value="{{ $data->nis }}">
            </div>
            <div class="mb-3">
              <label for="" class="form-label">Status</label>
              <select class="form-control" name="status" id="" style="height: 40px">
                  <option selected value="{{ $data->status }} ">{{ $data->status }}</option>
                 <option>Siswa sedang mencari tempat Prakerin</option>
                 <option>Siswa sedang mengajukan lembar pengesahan</option>
                 <option>Siswa siap untuk melakukan sidang</option>
              </select>
            </div>
            <div class="mb-3">
            <label for="" class="form-label">Presentase kehadiran</label>
            <input type="text" class="form-control" name="kehadiran" id="" value="{{ $data->kehadiran }}">
            </div>
            <div class="mb-3">
              <label for="" class="form-label">guru</label>
              <select class="form-control" name="guru_id" id="" style="height: 40px">
                  <option selected value="{{ $data->guru->id }}">{{ $data->guru->name }}</option>
                 @foreach ($data1 as $i)
                 <option value="{{ $i->id }}">{{  $i->name}}</option>
                 @endforeach 
              </select>
            </div>
            <div class="mb-3">
              <label for="" class="form-label">Pembimbing</label>
              <select class="form-control" name="pembimbing_id" id="" style="height: 40px">
                  <option selected value="{{ $data->pembimbing->id }}">{{ $data->pembimbing->name }}</option>
                 @foreach ($data2 as $i)
                 <option value="{{ $i->id }}">{{  $i->name}}</option>
                 @endforeach 
              </select>
            </div>
            <div class="mb-3">
            <label for="" class="form-label">comentar pembimbing sekolah</label>
                <textarea class="form-control" name="comentS" id="" rows="3" >{{ $data->comentS }}</textarea>
            </div>
            <div class="mb-3">
            <label for="" class="form-label">comentar pembimbing perusahaan</label>
                <textarea class="form-control" name="comentP" id="" rows="3" >{{ $data->comentP }}</textarea>
            </div>
        </div>
        </div>
        <div class="card">
        <div class="card-body">
         <h3>User Information</h3>
            <div class="mb-3">
              <label for="" class="form-label">Username</label>
              <input type="text" class="form-control" name="username" value="{{ $data->username }}">
            </div>
            <div class="mb-3">
              <label for="" class="form-label">email</label>
              <input type="email" class="form-control" name="email"  value="{{ $data->email }}">
            </div>
        </div>
        </div>
          <button type="submit" class="btn btn-primary">Submit</button>
 
    </form>
</div>
@endsection