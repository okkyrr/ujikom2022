@extends('layouts.admin.dashboard')

@section('body')

  <div class="row">
    <div class="col-lg-10 teks">

       <h4 class="text-uppercase text-center" style="margin-top: 150px">Sistem Informasi PKL ( SIP )</h3>
      <div class="row">
        <div class="col-lg-12 d-flex justify-content-center">
          <a href="/home" class="btn btn-lg mt-4 text-white" style="background-color: #EA2D2D">SMK Mutiara Kota Bandung</a>
        </div>
      </div>

      <div class="row">
        <div class="col-lg-12">
          <span class="d-flex" id="title" style="margin-top: 50px; margin-left: 78px">Login Sebagai :</span>
          <div class="row" id="body">
            <div class="col-lg-5 mt-5 d-flex flex-row tombol" style="margin-left: 78px">
              <a href="/login" class="btn text-white" style="text-decoration: none; background-color:#94B49F">Admin</a>
              <a href="/login" class="d-inline ml-5" style="text-decoration: none">Pembimbing</a>
              <a href="/login" class="ml-5" style="text-decoration: none">Siswa</a>
            </div>
          </div>
        </div>
      </div>
      
    </div>

    <div class="col-lg-1">

      <img src="/img/1.png" alt="" id="image" style="margin-top: 64px; margin-left: -350%; width: 450px">

    </div>
    
  </div>

@endsection