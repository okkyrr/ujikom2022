@extends('layouts.siswa.dashboard')

@section('body')

    <div class="container">
        <div class="card">
            <div class="card-body">
                <h3>Form Edit </h3><hr>
                <form action="/jurnal/update" method="post">
                    @csrf
                    @foreach ($data as $item)
                    <input type="hidden" name="id" value="{{ $item->id }}">
                    <input type="hidden" name="keterangan" value="">
                    <div class="mb-3">
                        <label for="" class="form-label">Tanggal :</label>
                        <input type="date"class="form-control" name="tanggal" value="{{ $item->tanggal }}">
                        <small id="helpId" class="form-text text-muted">Help text</small>
                    </div>
                    <div class="mb-3">
                        <label for="" class="form-label">Kegiatan :</label><br>   
                        <textarea name="kegiatan" id="" cols="30" rows="5">{{ $item->kegiatan }}</textarea>
                        <small id="helpId" class="form-text text-muted">Help text</small>
                    </div>
                    <div class="mb-3">
                        <label for="" class="form-label">Jam Masuk :</label>
                        <input type="time"class="form-control" name="jam_masuk" value="{{ $item->jam_masuk }}">
                        <small id="helpId" class="form-text text-muted">Help text</small>
                    </div>
                    <div class="mb-3">
                        <label for="" class="form-label">Jam keluar :</label>
                        <input type="time"class="form-control" name="jam_keluar" value="{{ $item->jam_keluar }}">
                        <small id="helpId" class="form-text text-muted">Help text</small>
                    </div>
                    <button type="submit" class="btn btn-primary" style="float: right">Submit</button>
                    @endforeach
                    <a href="/jurnal" class="btn btn-secondary">Back</a>
                </form>
            </div>
        </div>
    </div>    

@endsection